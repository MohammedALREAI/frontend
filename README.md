## Criteria

The sample application provides you with the following:

1. A Typescript project (see notes about Typescript below).
2. A `create-react-app` application using React 17 (see below for details).
3. Stubbed files and components for completing the work.

We want you to draw on your own experience and judgment when implementing these features. Please feel free to add libraries or change things as you see fit. We only have 2 technical requirements for your solution:

1. All code submitted must be written in Typescript
2. You must use React as the app's component library.

## Instructions

Please complete the following steps in order. As you are working through this test, we expect you to make tradeoffs given the time constraints. Please leave brief comments to explain your thinking and how you would do this differently if time weren't a constraint.

1. Currently, the product table is receiving all of its data from a JSON file called `sample-data.json`.
   We would like to load this data from a mock API instead. Please execute the `npm run serve` command in your terminal to start the mock server and then implement a mechanism to send a GET request to: `http://localhost:8080/resources`.

   1. Consider using the (currently empty) `src/api` module to implement this logic.

2. The page could use styling to improve the UX.

   1. Please give every other row a variation in color.
   2. Hovering on a row should highlight it a bit.

3. It would be really nice if a user could filter the rows of the table based on some search criteria. Please add a search field above the table that will filter rows based on text input from the user.
   1. The solution should support partial matching for any piece of text in the table. For example, if I enter the string 'mpu', it should match the word 'Compute'.
   2. The search should match against ANY column of the table. To use the previous example, the string 'mpu' would match 'Compute' in the Resource Type column and 'Accelerated Computing' in the Purpose column.
   3. Please try to make the search experience as pleasant for the user as possible while working within the time constraint. Perhaps the user doesn't want to see the table refreshing on every keystroke, or they might want to see a count of matched rows, or their search terms highlighted in the result. Again, use your best judgment, we don’t expect you to get to everything.

## Using Typescript

- You should be able to write plain Javascript inside `.ts` files, without needing to specify types.
- If you are creating React components, be sure to use the `.tsx` extension on files that contain JSX code
- We have provided some useful typings around the API data in `types.ts` -- this should be helpful in providing some autocompletion for data objects
- If you do prefer to work with Typescript, you may consider re-enabling the `strict` compiler flag in `tsconfig.json`

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm run serve`

Runs the mock API server\
Runs on `http://localhost:8080`

Supports `GET` calls at `http://localhost:8080/resources` .\
Returns a list of all available resources

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
